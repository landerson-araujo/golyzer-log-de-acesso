const inquirer = require('inquirer')
const moment = require('moment')

module.exports = {
  // Clientes do relatório.
  askClient: () => {
    const typesClient = [
      {
        name: 'typeClient',
        type: 'list',
        message: 'Para qual cliente deseja gerar o relatório',
        choices: ['Todos os clientes', 'Informar ID do cliente']
      }
    ]

    return inquirer.prompt(typesClient)
  },
  askClientId: () => {
    const clientId = [
      {
        name: 'clientId',
        type: 'input',
        message: 'Entre com o ID do cliente',
        validate: async (input) => {
          if (!input) {
            return 'Formato do ID do cliente inválido.'
          }

          return true
        }
      }
    ]

    return inquirer.prompt(clientId)
  },

  // Tipos de relatório.
  askType: () => {
    const types = [
      {
        name: 'types',
        type: 'list',
        message: 'Deseja gerar o relatório referente ao mês corrente ou deseja informar um mês',
        choices: ['Mês corrente', 'Informar mês', 'Cancelar (x)']
      }
    ]

    return inquirer.prompt(types)
  },
  askYearMonth: () => {
    const yearMonth = [
      {
        name: 'yearMonth',
        type: 'input',
        message: 'Entre com mês e ano do relatório (MM/YYYY)',
        validate: async (input) => {
          if (!moment(input, 'MM/YYYY').isValid()) {
            return 'Formato de mês e ano inválido.'
          }

          return true
        }
      }
    ]

    return inquirer.prompt(yearMonth)
  }
}
