const oracledb = require('oracledb')
const config = require('../config')
const moment = require('moment')
const XLSX = require('xlsx')

/**
 * Método responsável por interpretar os dados do banco de dados,
 * e em seguida monta o Excel baseado nesses mesmos dados.
 */
const mountExcel = (datas, file) => {
  console.info('Montando o excel...')

  // Montando os dados para gerar o Excel.
  datas.unshift(['Cliente', 'Usuário', 'Mensagem', 'Módulo', 'Data', 'Hora'])

  var wb = XLSX.utils.book_new(),
      ws = XLSX.utils.aoa_to_sheet(datas)

  XLSX.utils.book_append_sheet(wb, ws, 'Log de acesso')

  // Escrevendo informações adicionais no arquivo.
  wb.Props = {}
  wb.Props.Author = 'Landersson Almeida'
  wb.Props.Category = 'Log'
  wb.Props.Company = 'UpVarejo - developed by Landersson .A. Araújo'
  wb.Props.Language = 'pt-BR'

  // Gerando o relatório.
  XLSX.writeFile(wb, file + '.xlsx')

  // Informando que o relatório foi gerado com sucesso e informando o caminho do arquivo.
  console.info(`Relatório gerado com sucesso [ ${file.replace('./', '')} ] .`)
}

const getReport = async (date = null, clienteId = null) => {
  let connection

  try {
    /**
     * Iniciando a conexão com o banco de dados
     * Para obter os dados do relatório.
     */
    connection =  await oracledb.getConnection(config)

    // Recomendável que essa query se torne uma view.
    var query = `SELECT
      BE.NOME CLIENTE,
      BU.NOME USUARIO,
      BLS.MENSAGEM LOG,
      BTMLS.DESCRICAO MODULO,
      TO_CHAR(BLS.CREATEAT, 'DD/MM/YYYY') DATA,
      TO_CHAR(BLS.CREATEAT, 'HH24:MI:ss') HORA
    FROM BASE_LOG_SISTEMA BLS
    INNER JOIN BASE_ENTIDADES BE ON BLS.CLIENTE_ID = BE.ID
    INNER JOIN BASE_USUARIOS BU ON BLS.USUARIO_ID = BU.ID
    LEFT JOIN BASE_TIPO_LOG_SISTEMA BTMLS ON BLS.MODULO_ID = BTMLS.ID
    WHERE TO_CHAR(BLS.CREATEAT, 'MM/YYYY') = :dateC `,
    bindings = [date ? date : moment().format('MM/YYYY')]

    if (clienteId) {
      query += 'AND BLS.CLIENTE_ID = :clienteid'
      bindings.push(clienteId)
    }

    console.info('Buscando os dados...')

    const results = await connection.execute(query, bindings)

    /**
     * Verificando se há resultados para a query executada
     * se não houver resultados retorna um erro informando que não há dados.
     */
    if (!results.rows.length) {
      console.error('Nenhum log de acesso encontrado.')
      return false
    }

    // Montando o nome do arquivo.
    var file = `./relatorios/GolyZer Log de Acesso ${date ? date.replace('/', '-') : moment().format('MM-YYYY')}`
    if (clienteId) file += ' - ' + clienteId

    /**
     * Após obter os dados do relatório, devemos montar o Excel
     * Com as informações necessárias.
     */
    mountExcel(results.rows, file)
  } catch (err) {
    console.log(err)
  }
}

module.exports.getReport = getReport
