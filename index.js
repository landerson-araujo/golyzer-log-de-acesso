/**
 * Aplicação para gerar relatórios de log de acessos do GolyZer.
 *
 * @name golyzer-relatorio-acesso
 * @version 1.0.0
 * @author  Landersson Almeida
 * @copyright UpVarejo
 */
require('dotenv').config()

const clear = require('clear')
const inquirer = require('./src/lib/inquirer')

// Services
const serviceReport = require('./src/services/report')

clear()

const run = async () => {
  // Perguntando ao usuário como ele quer gerar o relatório.
  const client = await inquirer.askClient()

  var clientId = null
  if (client.typeClient == 'Informar ID do cliente') {
    const rClientId = await inquirer.askClientId()
    clientId = rClientId.clientId
  }

  const type = await inquirer.askType()

  switch (type.types) {
    // Caso for o mês corrente o selecionado, gera o relatório do mês atual.
    case 'Mês corrente':
      serviceReport.getReport(null, clientId)
      break

    // Se informado um mês, gera o relatório do mês informado.
    case 'Informar mês':
      const yearMonth = await inquirer.askYearMonth()
      serviceReport.getReport(yearMonth.yearMonth, clientId)
      break
  }
}

run()
