# GolyZer relatório log de acesso

Este projeto tem como objetivo facilitar a extreação de log's de acesso do banco de dados do projeto GolyZer.

## Instalação

1. Clonar esse repositório em qualquer diretório desejado
2. Navegue até a raiz do projeto
3. Rode `npm install` ou `yarn`
4. Crie um `.env` na raiz do projeto com as seguintes variáveis:
```
NODE_ORACLEDB_USER=
NODE_ORACLEDB_PASSWORD=
NODE_ORACLEDB_CONNECTIONSTRING={IP}/{SCHEMA}
```

## Uso

Para fazer o uso do sistema, basta rodar `node index.js` na raiz do projeto.

Depois basta seguir o fluxo do sistema, os relatórios gerados serão salvos em `/relatorios`.

## Licença

Desenvolvido para [UpVarejo Inteligência em tecnologia.](http://upvarejo.com.br)
